#!/bin/sh

FTP_SERVER=$FTP_SERVER
FTP_USER=$FTP_USER
FTP_PATH=$FTP_PATH
#NFS_SERVER=$NFS_SERVER
#NFS_PATH=$NFS_PATH

# mount -t nfs $NFS_SERVER:$NFS_PATH /mnt/nfs

echo "Deleting the old data"
rm -rf /mnt/beacon_data/*

echo "Start copying via FTP $FTP_PATH"
sftp -i /root/.ssh/id_rsa -o StrictHostKeyChecking=no $FTP_USER@$FTP_SERVER:$FTP_PATH /mnt/beacon_data
cd /mnt/beacon_data

echo "Unpacking the data"
tar -I zstd -xvf *.tar.zst

if [ $? -eq 0 ]; then
    echo "Deleting the tar.zst"
    rm *.tar.zst
else
   echo "Tar command failed. Not deleting the tar.zst file."
fi

echo "Total size of unpacked data"
du -sh /mnt/beacon_data


if [ -n "$DEBUG" ]; then
    echo "In debug mode, you need to kill manually the container"
    tail -f /dev/null
fi

# umount /mnt/beacon_data
