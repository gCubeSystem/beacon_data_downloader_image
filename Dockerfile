FROM alpine:latest

RUN apk update && \
    apk add --no-cache openssh-client zstd tar

# RUN mkdir -p /mnt/nfs

COPY copy_files.sh /usr/local/bin/copy_files.sh

RUN chmod +x /usr/local/bin/copy_files.sh

ENTRYPOINT ["/usr/local/bin/copy_files.sh"]

